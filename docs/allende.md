# allende

- runs beijaflor.es
- runs cloud.beijaflor.es (NextCloud)
- runs links.beijaflor.es
- **resources**:
  - 1 GB RAM / 25 GB Disk droplet
  - 100 GB external block storage volume
- **needs:**
  - git (version control)
  - NextCloud (Dropbox replacement, CalDAV + CardDAV server)
  - Apache2 (HTTPd/web server)
  - PostgreSQL (relational database)
  - zola (static site generator)
  - exa (ls replacement)
  - neovim (vim enhancement)
  - ruby (for now)
  - jekyll (for now)
