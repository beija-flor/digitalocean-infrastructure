# jaramillo

- ArtsyCraft Minecraft server
- pride.olivermattei.net
- **resources**:
  - 4 GB RAM / 80 GB Disk droplet
- **needs:**
  - git (version control)
  - OpenJDK JRE 11 (Java runtime)
  - Apache2 (HTTPd/web server)
  - PostgreSQL (relational database)
  - zola (static site generator)
  - exa (ls replacement)
  - neovim (vim enhancement)
